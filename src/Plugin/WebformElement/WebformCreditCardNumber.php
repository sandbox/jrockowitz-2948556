<?php

namespace Drupal\webform_creditcard\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'creditcard_number' element.
 *
 * @WebformElement(
 *   id = "webform_creditcard_number",
 *   label = @Translation("Credit card number"),
 *   description = @Translation("Provides a form element for entering a credit card number."),
 *   category = @Translation("Advanced elements"),
 * )
 */
class WebformCreditCardNumber extends WebformElementBase {

}
