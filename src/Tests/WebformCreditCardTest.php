<?php

namespace Drupal\webform_creditcard\Tests;

use Drupal\webform\Tests\WebformTestBase;

/**
 * Tests for creditcard elements.
 *
 * @group WebformCreditCard
 */
class WebformCreditCardTest extends WebformTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['webform', 'webform_creditcard', 'webform_creditcard_test'];

  /**
   * Test creditcard elements.
   */
  public function testWebformCreditCard() {
    /**************************************************************************/
    // creditcard_number
    /**************************************************************************/

    // Check basic creditcard_number.
    $this->drupalGet('webform/test_element_creditcard');
    $this->assertRaw('<label for="edit-creditcard-number-basic">creditcard_number_basic</label>');
    $this->assertRaw('<input data-drupal-selector="edit-creditcard-number-basic" type="text" id="edit-creditcard-number-basic" name="creditcard_number_basic" value="" size="16" maxlength="16" class="form-text webform-creditcard-number" />');

    // Check invalid credit card number.
    $edit = [
      'creditcard_number_basic' => 'not value',
    ];
    $this->drupalPostForm('webform/test_element_creditcard', $edit, t('Submit'));
    $this->assertRaw('The credit card number is not valid.');

    // Check valid credit card number.
    $edit = [
      'creditcard_number_basic' => '4111111111111111',
    ];
    $this->drupalPostForm('webform/test_element_creditcard', $edit, t('Submit'));
    $this->assertNoRaw('The credit card number is not valid.');

    // Check valid AmEx (15 digit).
    $edit = [
      'creditcard_number_basic' => '378282246310005',
    ];
    $this->drupalPostForm('webform/test_element_creditcard', $edit, t('Submit'));
    $this->assertNoRaw('The credit card number is not valid.');


    /**************************************************************************/
    // creditcard
    /**************************************************************************/

    // Check credit card.
    $this->drupalGet('webform/test_element_creditcard');
    $this->assertRaw('<div id="edit-creditcard-basic--wrapper" class="form-composite js-form-item form-item js-form-type-webform-creditcard form-type-webform-creditcard js-form-item-creditcard-basic form-item-creditcard-basic form-no-label">');
    $this->assertRaw('<label for="edit-creditcard-basic" class="visually-hidden">Credit Card</label>');
    $this->assertRaw('The credit card element is experimental and insecure because it stores submitted information as plain text.');
    $this->assertRaw('<label for="edit-creditcard-basic-name">Name on Card</label>');
    $this->assertRaw('<input data-drupal-selector="edit-creditcard-basic-name" type="text" id="edit-creditcard-basic-name" name="creditcard_basic[name]" value="John Smith" size="60" maxlength="255" class="form-text" />');

    /* Processing */

    // Check creditcard composite value.
    $this->drupalPostForm('webform/test_element_creditcard', [], t('Submit'));
    $this->assertRaw("creditcard_basic:
  name: 'John Smith'
  type: VI
  number: '4111111111111111'
  civ: '111'
  expiration_month: '1'
  expiration_year: '2025'");

  }

}
